/*
 Thermo Control 1206 - Differential Solar Hot Water Thermostat 

 The sketch is used in the Thermo Control controller with 1602 display. It 
 is intended for the use with a close loop solar system with a single pump and 
 two 10k NTC probes, one on solar panel and one on boiler.

 Circuit: 
 //TODO: Add circuit link

 Developed by: 
 Blagojce Kolicoski
*/

// include libraries
#include <EEPROM.h>
#include <LiquidCrystal.h>

// initialize the LCD interface pin
// with the arduino pin number it is connected to
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// output control pins
#define LCD_CONTRAST_PIN 6
#define LCD_BACKLIGHT_PIN 10
#define RELAY_PIN 7

// reading pins
#define TRM_BOILER A0
#define TRM_SOLAR A1
#define BUTTONS_PIN A5
#define BTN_HIGH 900
#define BTN_LOW 100

// NTC Settings
#define THERMISTORNOMINAL 10000 // resistance at 25 degrees C
#define TEMPERATURENOMINAL 25   // temp. for nominal resistance (almost always 25 C)
#define NUMSAMPLES 5            // how many samples to take and average, more takes longer but is more 'smooth'
#define BCOEFFICIENT 3950       // The beta coefficient of the thermistor (usually 3000-4000)
#define SERIESRESISTOR 10000   // the value of the 'other' resistor 

// set the epprom addresses for the settings that we are keeping
#define E_INIT         1023
// the setting array size must match the settings number
uint8_t settings[9];
// settings addresses
#define DELTA_T        0
#define T_HISTERESIS   1
#define T_BOILER_MAX   2
#define T_SOLAR_MAX    3
#define T_HOLIDAY      4
#define T_ANTI_FROST   5
#define T_SOLAR_MIN    6
#define LCD_CONTRAST   7
#define LCD_BACKLIGHT  8

// LCD Symbols
byte delta[8] = {B00000, B00000, B00100, B01010, B11111, B00000, B00000, B00000};

bool pumpState = false;
int buttons_pressed;

void setup() {
  Serial.begin(9600);
  loadSettings();
  setupPins();
  initLCD();
}

void loop() {

  buttons_pressed = analogRead(BUTTONS_PIN);
  Serial.print("Buttons: ");
  Serial.println(buttons_pressed);
  unsigned long time;
  int diff = 0;
  if (buttons_pressed < BTN_LOW) {
    //ACTION
    //debounce
    delay(200);
    time = millis() + 200;
    while(analogRead(BUTTONS_PIN) < BTN_LOW){
      diff = millis() - time;
      if (diff > 5000) {
        enterSetup();
      }
      delay(30);
    }
    String disp = String();
    disp = "Pressed action for " + disp;
    Serial.println(disp);
  } else if (buttons_pressed > BTN_HIGH) {
    //MODE
    delay(200);
    time = millis() + 200;
    while(analogRead(BUTTONS_PIN) > BTN_HIGH){
      delay(30);
    }
    String disp = String(millis() - time);
    disp = "Pressed mode for " + disp;
    Serial.println(disp);
  }

  float tmp_boiler;
  float tmp_solar;
  tmp_boiler = measureTemperature(TRM_BOILER);
  tmp_solar = measureTemperature(TRM_SOLAR);
 
  Serial.print("Temperature boiler "); 
  Serial.print(tmp_boiler);
  Serial.print(" *C - Temperature solar ");
  Serial.print(tmp_solar);
  Serial.println(" *C");

  
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 0);
  // print the number of seconds since reset:
  lcd.print("Tb ");
  lcd.print(tmp_boiler, 0);
  lcd.print((char)223);
  lcd.print("C  Ts ");
  lcd.print(tmp_solar, 0);
  lcd.print((char)223);
  lcd.print("C");

  
  if(round(tmp_solar) - round(tmp_boiler) > settings[DELTA_T] + settings[T_HISTERESIS]) {
    digitalWrite(RELAY_PIN, HIGH);
    pumpState = true;
  }
  if(round(tmp_solar) == round(tmp_boiler)){
    digitalWrite(RELAY_PIN, LOW);
    pumpState = false;
  }

  //update pump state
  lcd.setCursor(0, 1);
  lcd.print("P:");
  if (digitalRead(RELAY_PIN) == HIGH && !pumpState) {
    lcd.print("ON!");
  } else if (pumpState) {
    lcd.print("ON ");
  } else {
    lcd.print("OFF");
  }

  //Only valid for development and testing purposes
  //should be either removed or put very low
  delay(1000);
}

void enterSetup()
{
  unsigned long setupResetStart;
  lcd.clear();
  lcd.print("SETUP");
  //allow for button to be released
  delay(1000);
  setupResetStart = millis();
  int menuItem = 0;
  while(millis() - setupResetStart < 10000) {
    lcd.setCursor(0, 1);
    switch(menuItem) {
      case 0:
        lcd.print(" ");
        lcd.write(byte(0));
        lcd.print("t        ");
        if (settings[DELTA_T] < 9) {
          lcd.print(" ");
        }
        break; 
      case 1:
        lcd.print(" tHIST      ");
        break; 
      case 2:
        lcd.print(" tBMAX     ");
        break; 
      case 3:
        lcd.print(" tSMAX    ");
        break; 
      case 4:
        lcd.print(" tHOLIDAY  ");
        break; 
      case 5:
        lcd.print(" tAFROST  ");
        break; 
      case 6:
        lcd.print(" tSMIN     ");
        break; 
      case 7:
        lcd.print(" LCD CONT  ");
        break; 
      case 8:
        lcd.print(" LCD BACK  ");
        break;
      default:
        menuItem = 0;
        continue;  
    }
    if (menuItem == 5) {
      lcd.print((int8_t) settings[menuItem]);
    } else {
      lcd.print(settings[menuItem]);
    }
    if (menuItem < 7) {
      lcd.print((char)223);
      lcd.print("C");
    } else {
      lcd.print("  ");
    }
    buttons_pressed = analogRead(BUTTONS_PIN);
    if (buttons_pressed > BTN_HIGH) {
       setupResetStart = millis();
       settings[menuItem]++;

       switch(menuItem) {
        case 0:
          if (settings[DELTA_T] > 10) {
            settings[DELTA_T] = 2;
          }
          break; 
        case 1:
          if (settings[T_HISTERESIS] > 8) {
            settings[T_HISTERESIS] = 2;
          }
          break; 
        case 2:
          if (settings[T_BOILER_MAX] > 95) {
            settings[T_BOILER_MAX] = 10;
          }
          break; 
        case 3:
          if (settings[T_SOLAR_MAX] > 180) {
            settings[T_SOLAR_MAX] = 100;
          }
          break; 
        case 4:
          if (settings[T_HOLIDAY] > 70) {
            settings[T_HOLIDAY] = 10;
          }
          break; 
        case 5:
          if ((int8_t) settings[T_ANTI_FROST] > -10) {
            settings[T_ANTI_FROST] = -40;
          }
          break; 
        case 6:
          if (settings[T_SOLAR_MIN] > 70) {
            settings[T_SOLAR_MIN] = 10;
          }
          break; 
        case 7:
          if (settings[LCD_CONTRAST] > 8) {
            settings[LCD_CONTRAST] = 1;
          }
          analogWrite(LCD_CONTRAST_PIN, settings[LCD_CONTRAST] * 10);
          break; 
        case 8:
          if (settings[LCD_BACKLIGHT] > 20) {
            settings[LCD_BACKLIGHT] = 0;
          }
          analogWrite(LCD_BACKLIGHT_PIN, settings[LCD_BACKLIGHT] * 10);
          break;
        default:
          menuItem = 0;
          continue;  
      }
       
    } else if (buttons_pressed < BTN_LOW) {
      setupResetStart = millis();
      menuItem++;
    }
    delay(200);
  }
  //save settings on exit
  EEPROM.write(DELTA_T, settings[DELTA_T]);
  EEPROM.write(T_HISTERESIS, settings[T_HISTERESIS]);
  EEPROM.write(T_BOILER_MAX, settings[T_BOILER_MAX]);
  EEPROM.write(T_SOLAR_MAX, settings[T_SOLAR_MAX]);
  EEPROM.write(T_HOLIDAY, settings[T_HOLIDAY]);
  EEPROM.write(T_ANTI_FROST, settings[T_ANTI_FROST]);
  EEPROM.write(T_SOLAR_MIN, settings[T_SOLAR_MIN]);
  EEPROM.write(LCD_CONTRAST, settings[LCD_CONTRAST]);
  EEPROM.write(LCD_BACKLIGHT, settings[LCD_BACKLIGHT]);
  lcd.clear();
}

void loadSettings() {
  if (EEPROM.read(E_INIT) == 'T') {
    //settings have been initialized, read them
    settings[DELTA_T] = EEPROM.read(DELTA_T);
    settings[T_HISTERESIS] = EEPROM.read(T_HISTERESIS);
    settings[T_BOILER_MAX] = EEPROM.read(T_BOILER_MAX);
    settings[T_SOLAR_MAX] = EEPROM.read(T_SOLAR_MAX);
    settings[T_HOLIDAY] = EEPROM.read(T_HOLIDAY);
    settings[T_ANTI_FROST] = EEPROM.read(T_ANTI_FROST);
    settings[T_SOLAR_MIN] = EEPROM.read(T_SOLAR_MIN);
    settings[LCD_CONTRAST] = EEPROM.read(LCD_CONTRAST);
    settings[LCD_BACKLIGHT] = EEPROM.read(LCD_BACKLIGHT);
  } else {
    //first time run, settings were never set
    EEPROM.write(DELTA_T, 4);
    settings[DELTA_T] = 4;
    EEPROM.write(T_HISTERESIS, 4);
    settings[T_HISTERESIS] = 4;
    EEPROM.write(T_BOILER_MAX, 90);
    settings[T_BOILER_MAX] = 90;
    EEPROM.write(T_SOLAR_MAX, 130);
    settings[T_SOLAR_MAX] = 130;
    EEPROM.write(T_HOLIDAY, 30);
    settings[T_HOLIDAY] = 30;
    EEPROM.write(T_ANTI_FROST, -30);
    settings[T_ANTI_FROST] = -30;
    EEPROM.write(T_SOLAR_MIN, 35);
    settings[T_SOLAR_MIN] = 35;
    EEPROM.write(LCD_CONTRAST, 4);
    settings[LCD_CONTRAST] = 4;
    EEPROM.write(LCD_BACKLIGHT, 8);
    settings[LCD_BACKLIGHT] = 8;
    EEPROM.write(E_INIT, 'T');    
  }
}

void setupPins() {
  pinMode(LCD_CONTRAST_PIN, OUTPUT);
  analogWrite(LCD_CONTRAST_PIN, settings[LCD_CONTRAST] * 10);
  pinMode(LCD_BACKLIGHT_PIN, OUTPUT);
  analogWrite(LCD_BACKLIGHT_PIN, settings[LCD_BACKLIGHT] * 10);
  // the relay is active on low signal, but pump is on NC for safety
  // so activating the relay, turns off the pump 
  pinMode(RELAY_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, LOW);

  // connect AREF to 3.3V and use that as VCC, less noisy!
  analogReference(EXTERNAL);
}

void initLCD() {
  lcd.createChar(0, delta);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print(" Thermo Control");
  lcd.setCursor(5, 1);
  lcd.print("v. 1.0");
  delay(3000);
  lcd.clear();
}

float measureTemperature(int pinNumber) {
  uint8_t i;
  float average;
  int samples[NUMSAMPLES];
 
  // take N samples in a row, with a slight delay
  for (i=0; i< NUMSAMPLES; i++) {
   samples[i] = analogRead(pinNumber);
   delay(10);
  }
 
  // average all the samples out
  average = 0;
  for (i=0; i< NUMSAMPLES; i++) {
     average += samples[i];
  }
  average /= NUMSAMPLES;

  // convert the value to resistance
  average = 1023 / average - 1;
  average = SERIESRESISTOR / average;

  float steinhart;
  steinhart = average / THERMISTORNOMINAL;     // (R/Ro)
  steinhart = log(steinhart);                  // ln(R/Ro)
  steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
  steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
  steinhart = 1.0 / steinhart;                 // Invert
  steinhart -= 273.15;                         // convert to C

  return steinhart;
}

